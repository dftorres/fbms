<?php

namespace App\Http\Controllers;

class Users extends Controller
{
    /**
     * Return a list of existing users
     */
    public function index()
    {
        return response()->json([
            'status' => 200,
            'result' => [
                [
                    'id' => 1,
                    'username' => 'user1'
                ]
            ]
        ]);
    }
}
